import VueRouter from 'vue-router';

var routes = [
    { path: '*', redirect: '/' },
    {
        path: '/',
        component: require('./components/Dashboard'),
        meta: {

        }
    },
    {
        path: '/socialSprint',
        component: require('./components/socialSprint'),
        meta: {

        }
    },
    {
        path: '/manualScheduling',
        component: require('./components/manualScheduling'),
        meta: {

        }
    },
    {
        path: '/autoPilot',
        component: require('./components/autoPilot'),
        meta: {

        }
    },
    {
        path: '/socialSettings',
        component: require('./components/socialSettings'),
        meta: {

        }
    },
    {
        path: '/socialAnalytics',
        component: require('./components/socialAnalytics'),
        meta: {

        }
    },
    {
        path: '/singlePost',
        component: require('./components/singlePost'),
        meta: {

        }
    },
    {
        path: '/schedulePosts',
        component: require('./components/schedulePosts'),
        meta: {

        }
    },
    {
        path: '/generalSettings',
        component: require('./components/genSettings'),
        meta: {

        }
    },
    {
        path: '/audience-list',
        component: require('./components/audienceList'),
        meta: {

        }
    },
    {
        path: '/ca-builder',
        component: require('./components/caBuilder'),
        meta: {

        }
    },
    {
        path: '/laa-builder',
        component: require('./components/laaBuilder'),
        meta: {

        }
    },
    {
        path: '/eventStats',
        component: require('./components/eventStat'),
        meta: {

        }
    },
    {
        path: '/fbPixles',
        component: require('./components/fbPixels'),
        meta: {

        }
    },
    {
        path: '/manageCatalog',
        component: require('./components/manageCatalog'),
        meta: {

        }
    },
    {
        path: '/addCatalog',
        component: require('./components/addCatalog'),
        meta: {

        }
    },
    {
        path: '/googleCategory',
        component: require('./components/googleCategory'),
        meta: {

        }
    },
    {
        path: '/advancedSettings',
        component: require('./components/advancedSettings'),
        meta: {

        }
    },
    {
        path: '/settings',
        component: require('./components/settings'),
        meta: {

        }
    },
    {
        path: '/utmDefaults',
        component: require('./components/utmDefaults'),
        meta: {

        }
    },
    {
        path: '/maskingDomains',
        component: require('./components/maskingDomains'),
        meta: {

        }
    },
    {
        path: '/linkTags',
        component: require('./components/linkTags'),
        meta: {

        }
    },
    {
        path: '/manageLinks',
        component: require('./components/manageLinks'),
        meta: {

        }
    },
    {
        path: '/createLinks',
        component: require('./components/createLink'),
        meta: {

        }
    },
];

export default new VueRouter({
    routes: routes
});